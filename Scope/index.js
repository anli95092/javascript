/**
 * 1. 如果變數前面沒有 var ==> variable = value ==> 這裡的變數都是指全域變數
 *    就算在 function 裡面也是一樣，所以一定要加變數定義的前綴
 *
 *
 *
 */
//1. 沒有 var 前綴的狀況-------------------------------------------------------------------------------------------------

//  全域變數
name = "Brian";

//  name 也是指到全域變數，會直接覆蓋
function getName() {
  var name = "Jerry";
  return name;
}

console.log("first:", name); //  Brian
console.log(getName()); // Jerry
console.log("Second:", name); // Jerry

//2. 沒有 var 前綴的狀況-------------------------------------------------------------------------------------------------
